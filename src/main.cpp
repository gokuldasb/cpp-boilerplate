// Copyright (C) 2021 Gokul Das B
// SPDX-License-Identifier: MIT

#include <fmt/format.h>
#include<string>

int main() {
  std::wstring wst = L"World";
  fmt::print(L"Hello {}!\n", wst);
}
