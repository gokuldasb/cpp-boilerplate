;; Guix manifest for development environment
(specifications->manifest
 '(;; Base environment
   "busybox"
   ;; The following lets meson download packages
   "openssl"  
   "nss-certs"

   ;; Build tools
   "gcc-toolchain"
   "meson"
   "ninja"
   "pkg-config"

   ;; Build-time (static) dependencies

   ;; Runtime (dynamic) dependencies
   "fmt@7.1"))
