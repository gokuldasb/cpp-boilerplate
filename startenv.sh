#!/usr/bin/env bash

# Start guix container for build, with required services
# Make sure to be in the project root directory

guix environment \
    --container \
    --network \
    --preserve='^DISPLAY$' \
    --preserve='^TERM$' \
    --expose=/etc/ssl/certs/ \
    --manifest=manifest.scm
